use clap::Parser;

/// The Primary Worker to scan for partitions using two hop circuits
#[derive(Parser, Debug, Clone)]
#[command(version, about)]
pub struct Args {
    /// The path to the config file for the Primary Worker
    #[arg(short, long, default_value = "Config.toml")]
    pub config: String,

    /// The path to the environment file for the Primary Worker
    #[arg(short, long, default_value = ".env")]
    pub env: String,

    /// The path to log config of log4rs
    #[arg(short, long, default_value = "log_config.yml")]
    pub log_config: String,

    /// The path to the sqlite3 database to resume the worker from previous state
    #[arg(short, long)]
    pub resume: Option<String>,
}
